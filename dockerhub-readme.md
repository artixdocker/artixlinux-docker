# Quick reference

* **Maintained by**:

  [Cory Sanin](https://artixlinux.org/kotnr.php#Cory%20Sanin), [artoo](https://artixlinux.org/kotor.php#Marcus%20von%20Ditfurth), [Chris Cromer](https://artixlinux.org/kotor.php#Chris%20Cromer)

* **Where to get help**:

  [Artix Wiki](https://wiki.artixlinux.org/), [Artix Linux Forum](https://forum.artixlinux.org/)

# Supported tags

* latest, base
* base-devel
* base-dinit
* base-openrc
* base-runit
* base-s6

## Dockerfile repo

[artixlinux-docker](https://gitea.artixlinux.org/artixdocker/artixlinux-docker)

# Purpose

* Provide Artix Linux in a Docker Image
* Provide the most simple but complete image to base every other upon
* `pacman` needs to work out of the box
* All installed packages have to be kept unmodified


# How to run

```
docker pull artixlinux/artixlinux:base
docker run -it --rm artixlinux/artixlinux:base /bin/bash
```

Note that `--rm` removes the container on exit.